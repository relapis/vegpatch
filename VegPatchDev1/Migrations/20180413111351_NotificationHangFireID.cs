﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VegPatchDev1.Migrations
{
    public partial class NotificationHangFireID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EventTime",
                table: "ScheduledNotifications");

            migrationBuilder.AddColumn<string>(
                name: "HangFireJobID",
                table: "ScheduledNotifications",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HangFireJobID",
                table: "ScheduledNotifications");

            migrationBuilder.AddColumn<DateTime>(
                name: "EventTime",
                table: "ScheduledNotifications",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
