﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VegPatchDev1.Migrations
{
    public partial class TaskSortGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "TimerTaskID",
                table: "PlantTimers",
                nullable: false,
                oldClrType: typeof(byte));

            migrationBuilder.AddColumn<string>(
                name: "SortGroup",
                table: "PlantTasks",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AlarmTaskID",
                table: "PlantAlarms",
                nullable: false,
                oldClrType: typeof(byte));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SortGroup",
                table: "PlantTasks");

            migrationBuilder.AlterColumn<byte>(
                name: "TimerTaskID",
                table: "PlantTimers",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<byte>(
                name: "AlarmTaskID",
                table: "PlantAlarms",
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}
