﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Hangfire;
using VegPatchDev1.Data;
using Microsoft.EntityFrameworkCore;

namespace VegPatchDev1
{
    public static class NotificationHandler
    {
        public enum NotificationTypes
        {
            PUSH = 0,
            EMAIL = 1,
            TEXT = 2
        }
        public static readonly EnumBoolIndexer<NotificationTypes> EnabledTypesOfNotifications = new EnumBoolIndexer<NotificationTypes>(NotificationTypes.PUSH, NotificationTypes.EMAIL);

        public class NotificationCall
        {
            public EventTypes type;
            public ScheduledNotification notification;
            public string UserID;
            public Plot plot;
            public PlantTimers timer;
            public PlantAlarms alarm;
        }
        
        public static NotificationCall GetNotificationCall(long NotificationID)
        {
            return (from n in _context.ScheduledNotifications
                    join p in _context.UserPlots
                    on n.PlotID equals p.id
                    join a in _context.PlantAlarms
                    on n.EventID equals a.id
                    join g in _context.UserGardens
                    on p.GardenID equals g.id
                    where n.id.Equals(NotificationID)
                    select new NotificationCall()
                    {
                        UserID = g.UserID,
                        type = n.EventType,
                        notification = n,
                        plot = p,
                        alarm = a
                    }).FirstOrDefault();
        }
        public static ApplicationDbContext _context { get; set; }

        public static string GetUserIDFromGardenID(int gardenID)
        {
            return (from g in _context.UserGardens
                    where g.id.Equals(gardenID)
                    select g.UserID).FirstOrDefault();
        }

        public static async Task<IEnumerable<ScheduledNotification>> GetNotifications(Func<ScheduledNotification, bool> Query)
        {
            return await _context.ScheduledNotifications.Where((n) => Query(n)).ToListAsync();
        }
        public static async Task CancelNotifications(Func<ScheduledNotification, bool> Query)
        {
            IEnumerable<ScheduledNotification> toDelete = await GetNotifications(Query);
            foreach (ScheduledNotification n in toDelete)
            {
                BackgroundJob.Delete(n.HangFireJobID);
            }
            _context.ScheduledNotifications.RemoveRange(toDelete);
            await _context.SaveChangesAsync();
        }
        public static async Task<IEnumerable<ScheduledNotification>> GetNotification(int id)
        {
            return await GetNotifications((n) => n.id.Equals(id));
        }
        public static async Task CancelNotification(int id)
        {
            await CancelNotifications((n) => n.id.Equals(id));
        }
        public static async Task<IEnumerable<ScheduledNotification>> GetNotificationsForPlot(int plotID)
        {
            return await GetNotifications((n) => n.PlotID.Equals(plotID));
        }
        public static async Task CancelNotificationsForPlot(int plotID)
        {
            await CancelNotifications((n) => n.PlotID.Equals(plotID));
        }
        public static async Task<IEnumerable<ScheduledNotification>> GetNotificationsForUser(string userID)
        {
            return await GetNotifications((n) => n.UserID.Equals(userID));
        }
        public static async Task CancelNotificationsForUser(string userID)
        {
            await CancelNotifications((n) => n.UserID.Equals(userID));
        }

        public delegate void OnCallDel(NotificationCall notification, string options);
        public interface INotificationHandler<T, P>
        {
            event OnCallDel OnCalled;

            Task<long> Schedule(T t, P p, string options = "", short? EmailTemplate = null);
            Task ScheduleToList(List<long> output, T t, P p, string options = "", short? EmailTemplate = null);
            Task<List<long>> ScheduleRange(List<ValueTuple<T,P>> values, string options = "", short? EmailTemplate = null);
            Task<List<long>> ScheduleRange(Task<List<ValueTuple<T, P>>> values, string options = "", short? EmailTemplate = null);

            void Call(long NotificationID, string options);

            Task<IEnumerable<ScheduledNotification>> Get();

            Task Cancel(T t);
        }


        public static INotificationHandler<string,DateTime> miscellaneous = new MISCELLANEOUS();
        public class MISCELLANEOUS : INotificationHandler<string, DateTime>
        {
            public event OnCallDel OnCalled;

            public void Call(long NotificationID, string message)
            {
                NotificationCall found = GetNotificationCall(NotificationID);

                if (found != null)
                {
                    OnCalled.Invoke(found, message);
                }
            }

            public async Task Cancel(string userID)
            {
                await CancelNotificationsForUser(userID);
            }

            public async Task<IEnumerable<ScheduledNotification>> Get()
            {
                return await GetNotifications((n) => n.EventType == EventTypes.MISC);
            }

            public async Task<long> Schedule(string userID, DateTime p, string message = "", short? EmailTemplate = null)
            {
                ScheduledNotification s = new ScheduledNotification()
                {
                    EventType = EventTypes.MISC,
                    UserID = userID
                };
                if (EmailTemplate != null)
                    s.EmailTemplateId = EmailTemplate.Value;

                _context.Add(s);
                await _context.SaveChangesAsync();

                s.HangFireJobID = BackgroundJob.Schedule(() => Call(s.id, message), p);

                await _context.SaveChangesAsync();
                return s.id;
            }

            public async Task<List<long>> ScheduleRange(List<ValueTuple<string, DateTime>> values, string options = "", short? EmailTemplate = null)
            {
                var output = new List<long>();
                var tasks = new List<Task>();
                foreach(var v in values)
                {
                     tasks.Add(Task.Run(async () => ScheduleToList(output, v.Item1, v.Item2, options, EmailTemplate)));
                }
                Task.WaitAll(tasks.ToArray());
                return output;
            }

            public async Task<List<long>> ScheduleRange(Task<List<ValueTuple<string, DateTime>>> values, string options = "", short? EmailTemplate = null)
            {
                return await ScheduleRange(await values, options, EmailTemplate);
            }

            public async Task ScheduleToList(List<long> output, string t, DateTime p, string options = "", short? EmailTemplate = null)
            {
                output.Add(await Schedule(t, p, options, EmailTemplate));
            }
        }



        public static INotificationHandler<PlantTimers,Plot> timers = new TIMERS();
        public class TIMERS : INotificationHandler<PlantTimers,Plot>
        {
            public TIMERS()
            {
                OnCalled += RescheduleTimer;
            }

            public event OnCallDel OnCalled;

            public void Call(long NotificationID, string options)
            {
                NotificationCall found = GetNotificationCall(NotificationID);

                if (found != null)
                {
                    OnCalled.Invoke(found,options);
                }
            }

            public async Task Cancel(PlantTimers t)
            {
                await CancelNotifications((n) => n.EventType == EventTypes.TIMER && n.EventID == t.id);
            }

            public async Task<IEnumerable<ScheduledNotification>> Get()
            {
                return await GetNotifications((n) => n.EventType == EventTypes.TIMER);
            }

            void RescheduleTimer(NotificationCall n, string options)
            {
                string parameters = options;
                if (!options.Contains("-r"))
                    parameters += " -r";
                Task.Run(() => Schedule(n.timer, n.plot, parameters));
            }
            public async Task<long> Schedule(PlantTimers t, Plot p, string parameters="", short? EmailTemplate=null)
            {
                DateTime EventTime;
                if (!parameters.Contains("-r"))
                    EventTime = p.StartTime + t.TimerOffset;
                else
                    EventTime = DateTime.Now + t.TimerInterval;
                string userID = GetUserIDFromGardenID(p.GardenID);
                ScheduledNotification s = new ScheduledNotification()
                {
                    PlotID = p.id,
                    EventType = EventTypes.TIMER,
                    EventID = t.id,
                    UserID = userID
                };
                if (EmailTemplate != null)
                    s.EmailTemplateId = EmailTemplate.Value;

                _context.Add(s);
                await _context.SaveChangesAsync();

                s.HangFireJobID = BackgroundJob.Schedule(() => Call(s.id, parameters), EventTime);

                await _context.SaveChangesAsync();
                return s.id;
            }
            public async Task ScheduleToList(List<long> output, PlantTimers t, Plot p, string parameters = "", short? EmailTemplate = null)
            {
                output.Add(await Schedule(t, p, parameters, EmailTemplate));
            }
            public async Task<List<long>> ScheduleRange(List<ValueTuple<PlantTimers, Plot>> values, string options = "", short? EmailTemplate = null)
            {
                var output = new List<long>();
                var tasks = new List<Task>();
                foreach (var v in values)
                {
                    tasks.Add(Task.Run(async () => ScheduleToList(output, v.Item1, v.Item2, options, EmailTemplate)));
                }
                Task.WaitAll(tasks.ToArray());
                return output;
            }
            public async Task<List<long>> ScheduleRange(Task<List<ValueTuple<PlantTimers, Plot>>> values, string options = "", short? EmailTemplate = null)
            {
                return await ScheduleRange(await values, options, EmailTemplate);
            }
        }


        public static INotificationHandler<PlantAlarms,Plot> alarms = new ALARMS();
        public class ALARMS : INotificationHandler<PlantAlarms,Plot>
        {
            public event OnCallDel OnCalled;

            public void Call(long NotificationID, string options)
            {
                NotificationCall found = GetNotificationCall(NotificationID);

                if (found != null)
                {
                    OnCalled.Invoke(found, options);
                }
            }

            public async Task Cancel(PlantAlarms t)
            {
                await CancelNotifications((n) => n.EventType == EventTypes.ALARM && n.EventID == t.id);
            }

            public async Task<IEnumerable<ScheduledNotification>> Get()
            {
                return await GetNotifications((n) => n.EventType == EventTypes.ALARM);
            }

            public async Task<long> Schedule(PlantAlarms t, Plot p, string parameters = "", short? EmailTemplate = null)
            {
                DateTime EventTime = p.StartTime + t.AlarmTime;
                string userID = GetUserIDFromGardenID(p.GardenID);
                ScheduledNotification s = new ScheduledNotification()
                {
                    PlotID = p.id,
                    EventType = EventTypes.TIMER,
                    EventID = t.id,
                    UserID = userID
                };
                if (EmailTemplate != null)
                    s.EmailTemplateId = EmailTemplate.Value;

                _context.Add(s);
                await _context.SaveChangesAsync();

                s.HangFireJobID = BackgroundJob.Schedule(() => Call(s.id, parameters), EventTime);

                await _context.SaveChangesAsync();
                return s.id;
            }
            public async Task<List<long>> ScheduleRange(List<ValueTuple<PlantAlarms, Plot>> values, string options = "", short? EmailTemplate = null)
            {
                var output = new List<long>();
                var tasks = new List<Task>();
                foreach (var v in values)
                {
                    tasks.Add(Task.Run(async () => ScheduleToList(output, v.Item1, v.Item2, options, EmailTemplate)));
                }
                Task.WaitAll(tasks.ToArray());
                return output;
            }
            public async Task<List<long>> ScheduleRange(Task<List<ValueTuple<PlantAlarms, Plot>>> values, string options = "", short? EmailTemplate = null)
            {
                return await ScheduleRange(await values, options, EmailTemplate);
            }

            public async Task ScheduleToList(List<long> output, PlantAlarms t, Plot p, string options = "", short? EmailTemplate = null)
            {
                output.Add(await Schedule(t, p, options, EmailTemplate));
            }
        }
    }
}
