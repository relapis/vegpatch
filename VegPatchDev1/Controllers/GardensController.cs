﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VegPatchDev1;
using VegPatchDev1.Data;
using Microsoft.AspNetCore.Authorization;

namespace VegPatchDev1.Controllers
{
    [Authorize]
    public class GardensController : Controller
    {
        private readonly ApplicationDbContext _context;

        public GardensController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Gardens
        public async Task<IActionResult> Index()
        {
            return View(await(from garden in _context.UserGardens
                              where garden.UserID == User.GetUserId()
                              select garden.ToViewModel(_context.UserPlots.Count((p) => p.GardenID == garden.id))).ToListAsync());
        }

        // GET: Gardens/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var garden = await _context.UserGardens
                .SingleOrDefaultAsync(m => m.id == id);
            if (garden == null)
            {
                return NotFound();
            }

            return View(garden);
        }

        // GET: Gardens/Create
        public IActionResult Create()
        {
            ViewData["NameExists"] = false;
            return View();
        }

        // POST: Gardens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,Name,UserID")] Models.GardenShow garden)
        {
            ViewData["NameExists"] = false;
            if (ModelState.IsValid)
            {
                garden.UserID = User.GetUserId();
                if (!garden.CanBeCreated())
                {
                    ViewData["NameExists"] = true;
                    return View(garden);
                }
                _context.Add(garden.ToBase());
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(garden);
        }

        // GET: Gardens/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            if(!User.UserHasAccessToGarden(id.Value))
            {
                return View();
            }

            var garden = await _context.UserGardens.SingleOrDefaultAsync(m => m.id == id.Value);
            if (garden == null)
            {
                return NotFound();
            }
            return View(garden);
        }

        // POST: Gardens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,Name,UserID")] Models.GardenShow garden)
        {
            if (id != garden.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(garden.ToBase());
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GardenExists(garden.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(garden);
        }

        // GET: Gardens/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var garden = await _context.UserGardens
                .SingleOrDefaultAsync(m => m.id == id);
            if (garden == null)
            {
                return NotFound();
            }

            return View(garden.ToViewModel(_context.UserPlots.Count((p) => p.GardenID == garden.id)));
        }

        // POST: Gardens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var garden = await _context.UserGardens.SingleOrDefaultAsync(m => m.id == id);
            _context.UserGardens.Remove(garden);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GardenExists(int id)
        {
            return _context.UserGardens.Any(e => e.id == id);
        }
    }
}
