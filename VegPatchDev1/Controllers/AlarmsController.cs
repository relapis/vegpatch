﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VegPatchDev1;
using VegPatchDev1.Data;

namespace VegPatchDev1.Controllers
{
    public class AlarmsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AlarmsController(ApplicationDbContext context)
        {
            _context = context;
            IDQuery = (id) =>
            {
                return (
                from alarm in _context.PlantAlarms
                join plant in _context.PlantData
                on alarm.PlantID equals plant.id
                join task in _context.PlantTasks
                on alarm.AlarmTaskID equals task.id
                where alarm.PlantID.Equals(id)
                select alarm.ToViewModel(plant.PlantName, task.TaskName)
                );
            };
        }

        // GET: Alarms
        public async Task<IActionResult> Index(int? id)
        {
            if (id.HasValue == false)
                return NotFound();
            return View(await IDQuery(id.Value).ToListAsync());
        }

        public readonly Func<int, IQueryable<Models.AlarmShow>> IDQuery;

        // GET: Alarms/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var plantAlarms = await IDQuery(id.Value).SingleOrDefaultAsync();
            if (plantAlarms == null)
            {
                return NotFound();
            }
            return View(plantAlarms);
        }

        // GET: Alarms/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Alarms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,PlantID,AlarmTime,AlarmDetails,AlarmTaskID")] Models.AlarmShow plantAlarms)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plantAlarms.ToBase());
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { id = plantAlarms.PlantID });
            }
            return View(plantAlarms);
        }

        // GET: Alarms/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plantAlarms = await IDQuery(id.Value).SingleOrDefaultAsync();
            if (plantAlarms == null)
            {
                return NotFound();
            }
            return View(plantAlarms);
        }

        // POST: Alarms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,PlantID,AlarmTime,AlarmDetails,AlarmTaskID")] Models.AlarmShow plantAlarms)
        {
            if (id != plantAlarms.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plantAlarms.ToBase());
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlantAlarmsExists(plantAlarms.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { id = plantAlarms.id });
            }
            return View(plantAlarms);
        }

        // GET: Alarms/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plantAlarms = await IDQuery(id.Value).SingleOrDefaultAsync();
            if (plantAlarms == null)
            {
                return NotFound();
            }
            return View(plantAlarms);
        }

        // POST: Alarms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plantAlarms = await _context.PlantAlarms.SingleOrDefaultAsync(m => m.id == id);
            _context.PlantAlarms.Remove(plantAlarms);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlantAlarmsExists(int id)
        {
            return _context.PlantAlarms.Any(e => e.id == id);
        }
    }
}
