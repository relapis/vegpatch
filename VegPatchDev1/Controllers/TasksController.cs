﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VegPatchDev1;
using VegPatchDev1.Data;
using Microsoft.AspNetCore.Authorization;

namespace VegPatchDev1.Controllers
{
    public class TasksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TasksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Tasks
        public async Task<IActionResult> Index()
        {
            return View(await _context.PlantTasks.ToListAsync());
        }

        // GET: Tasks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plantTasks = await _context.PlantTasks
                .SingleOrDefaultAsync(m => m.id == id);
            if (plantTasks == null)
            {
                return NotFound();
            }

            return View(plantTasks);
        }

        // GET: Tasks/Create
        [Authorize(Roles = "DBAdmin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Tasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,TaskName,TaskDescription,SortGroup")] PlantTasks plantTasks)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plantTasks);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(plantTasks);
        }

        // GET: Tasks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plantTasks = await _context.PlantTasks.SingleOrDefaultAsync(m => m.id == id);
            if (plantTasks == null)
            {
                return NotFound();
            }
            return View(plantTasks);
        }

        // POST: Tasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,TaskName,TaskDescription,SortGroup")] PlantTasks plantTasks)
        {
            if (id != plantTasks.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plantTasks);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlantTasksExists(plantTasks.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(plantTasks);
        }

        // GET: Tasks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plantTasks = await _context.PlantTasks
                .SingleOrDefaultAsync(m => m.id == id);
            if (plantTasks == null)
            {
                return NotFound();
            }

            return View(plantTasks);
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plantTasks = await _context.PlantTasks.SingleOrDefaultAsync(m => m.id == id);
            _context.PlantTasks.Remove(plantTasks);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlantTasksExists(int id)
        {
            return _context.PlantTasks.Any(e => e.id == id);
        }
    }
}
