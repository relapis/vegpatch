﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VegPatchDev1;
using VegPatchDev1.Data;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using System.Runtime.CompilerServices;

namespace VegPatchDev1.Controllers
{
    [Authorize]
    public class PlotsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlotsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Plots
        public async Task<IActionResult> Index(int? id)
        {
            string userId = User.GetUserId();
            if (_context.UserGardens.FirstOrDefault((g) => g.UserID.Equals(userId) && (id == null || g.id.Equals(id.Value))) == null)
                return NotFound();
            return View(await (from plot in _context.UserPlots
                               join garden in _context.UserGardens
                               on plot.GardenID equals garden.id
                               join plant in _context.PlantData
                               on plot.PlantID equals plant.id
                               where garden.UserID.Equals(userId) && (id == null || garden.id.Equals(id.Value))
                               select plot.ToViewModel(garden.Name,plant.PlantName)).ToListAsync());
        }

        // GET: Plots/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var plotShow = await (from p in _context.UserPlots
                         join garden in _context.UserGardens
                         on p.GardenID equals garden.id
                         join plant in _context.PlantData
                         on p.PlantID equals plant.id
                         where p.id.Equals(id.Value)
                         select new Models.PlotShow(p,garden.Name,plant.PlantName)).SingleOrDefaultAsync();
            if (plotShow == null)
            {
                return NotFound();
            }

            return View(plotShow);
        }

        // GET: Plots/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Plots/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,Name,GardenID,PlantID,PlotArea,StartTime,InitialSeedCount")] Plot plot)
        {
            if (ModelState.IsValid)
            {
                PlotToNotifications(plot).Start();
                _context.Add(plot);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new{ id = plot.GardenID });
            }
            return View(plot);
        }

        public async Task PlotToNotifications(Plot p)
        {
            var timerAsync = (from timer in _context.PlantTimers
                          where timer.PlantID.Equals(p.PlantID)
                          select new ValueTuple<PlantTimers, Plot>(timer, p)).ToListAsync();
            var alarmAsync = (from alarm in _context.PlantAlarms
                          where alarm.PlantID.Equals(p.PlantID)
                          select new ValueTuple<PlantAlarms, Plot>(alarm, p)).ToListAsync();

            Task.WaitAll(timerAsync, alarmAsync);

            Task.Run(() => NotificationHandler.timers.ScheduleRange(timerAsync));
            Task.Run(() => NotificationHandler.alarms.ScheduleRange(alarmAsync));
        }

        // GET: Plots/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plot = await _context.UserPlots.SingleOrDefaultAsync(m => m.id == id);
            if (plot == null && User.UserHasAccessToPlot(plot.id))
            {
                return NotFound();
            }
            return View(plot);
        }

        // POST: Plots/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,Name,GardenID,PlantID,PlotArea,StartTime,InitialSeedCount")] Plot plot)
        {
            if (id != plot.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (User.UserHasAccessToPlot(plot.id))
                    {
                        _context.Update(plot);
                        await _context.SaveChangesAsync();
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlotExists(plot.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { id = plot.GardenID });
            }
            return View(plot);
        }

        // GET: Plots/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plot = await _context.UserPlots
                .SingleOrDefaultAsync(m => m.id == id);
            if (plot == null)
            {
                return NotFound();
            }

            return View(plot);
        }

        // POST: Plots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plot = await _context.UserPlots.SingleOrDefaultAsync(m => m.id == id);
            if (User.UserHasAccessToPlot(id))
            {
                _context.UserPlots.Remove(plot);
                await _context.SaveChangesAsync();
            }
            return RedirectToAction(nameof(Index));
        }

        private bool PlotExists(int id)
        {
            return _context.UserPlots.Any(e => e.id == id);
        }
    }
}
