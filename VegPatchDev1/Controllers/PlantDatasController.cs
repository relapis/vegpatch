﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VegPatchDev1;
using VegPatchDev1.Data;
using Microsoft.AspNetCore.Authorization;

namespace VegPatchDev1.Controllers
{
    public class PlantDatasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlantDatasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PlantDatas
        public async Task<IActionResult> Index(bool? IncludeWeeds, bool? ExcludeNormal)
        {
            //Include weeds -> do weeds
            bool weeds = IncludeWeeds.HasValue ? IncludeWeeds.Value : false;
            //Exclude normal -> do normal
            bool normal = ExcludeNormal.HasValue ? !ExcludeNormal.Value : true;

            return View(await _context.PlantData.Where((p) => (p.IsAWeed && weeds) || (!p.IsAWeed && normal)).ToListAsync());
        }

        // GET: PlantDatas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plantData = await _context.PlantData
                .SingleOrDefaultAsync(m => m.id == id);
            if (plantData == null)
            {
                return NotFound();
            }

            return View(plantData);
        }

        // GET: PlantDatas/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlantDatas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("id,PlantName,PlantDescription,ExpectedGrowthTime,IsAWeed,Image,HarvestInstructions,HarvestType,PlantInstructions")] PlantData plantData)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plantData);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(plantData);
        }

        // GET: PlantDatas/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plantData = await _context.PlantData.SingleOrDefaultAsync(m => m.id == id);
            if (plantData == null)
            {
                return NotFound();
            }
            return View(plantData);
        }

        // POST: PlantDatas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("id,PlantName,PlantDescription,ExpectedGrowthTime,IsAWeed,Image,HarvestInstructions,HarvestType,PlantInstructions")] PlantData plantData)
        {
            if (id != plantData.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plantData);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlantDataExists(plantData.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(plantData);
        }

        // GET: PlantDatas/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plantData = await _context.PlantData
                .SingleOrDefaultAsync(m => m.id == id);
            if (plantData == null)
            {
                return NotFound();
            }

            return View(plantData);
        }

        // POST: PlantDatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plantData = await _context.PlantData.SingleOrDefaultAsync(m => m.id == id);
            _context.PlantData.Remove(plantData);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlantDataExists(int id)
        {
            return _context.PlantData.Any(e => e.id == id);
        }
    }
}
