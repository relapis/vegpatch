﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace VegPatchDev1
{
    #region CONSTANT DATA
    /// <summary>
    /// Detailed information on the plants
    /// </summary>
    public class PlantData
    {
        [Key]
        public int id { get; set; }

        /// <summary>
        /// The name of the plant.
        /// </summary>
        [Required][Display(Name = "Plant Name")]
        public string PlantName { get; set; }

        /// <summary>
        /// A description of the plant
        /// </summary>
        [Display(Name = "Plant Description")]
        public string PlantDescription { get; set; }

        /// <summary>
        /// The expected growth time of the plant in days.
        /// </summary>
        [Display(Name = "Expected Time", Description = "The Expected time of growth in days.")]
        public short ExpectedGrowthTime { get; set; }

        /// <summary>
        /// Is the plant classified as a weed?
        /// </summary>
        [Display(Name = "Is a weed", Description = "Is the plant classed as a weed?")]
        public bool IsAWeed { get; set; }

        /// <summary>
        /// Byte array of an image of the plant.
        /// </summary>
        [Display(Name = "Image", Description = "Directory Path to an image.")]
        public string ImagePath { get; set; }

        /// <summary>
        /// Instructions for what to do when harvesting the plant.
        /// </summary>
        [Display(Name = "Harvest Guidelines", Description = "Guidelines or instructions on how to harvest the plant.")]
        public string HarvestInstructions { get; set; }

        /// <summary>
        /// The type of harvesting. (See HarvestTypes enum)
        /// </summary>
        [Display(Name = "Harvest Type", Description = "What type of plant harvest is it? Does the plant need to be replanted?")]
        public HarvestTypes HarvestType { get; set; }

        /// <summary>
        /// Instructions of what to do when planting the plant.
        /// </summary>
        [Display(Name = "Planting Guidelines", Description = "Guidelines or instructions on how to plant / replant the plant.")]
        public string PlantInstructions { get; set; }
    }
    /// <summary>
    /// Reccuring events for notifications
    /// </summary>
    public class PlantTimers
    {
        [Key]
        public int id { get; set; }

        /// <summary>
        /// ID of the plant the timer is for.
        /// </summary>
        [Required]
        [Display(Name = "Plant", Description = "The plant this timer refers to.")]
        public int PlantID { get; set; }

        /// <summary>
        /// Initial offset to start from for the timer interval.
        /// I.E. the first time after the planting the timer should be called, will then use interval afterwords.
        /// </summary>
        public TimeSpan TimerOffset { get; set; }

        /// <summary>
        /// Interval in hours for the timer to repeat. Starts from initial offset.
        /// </summary>
        public TimeSpan TimerInterval { get; set; }

        /// <summary>
        /// Description of what to do when this timer activates.
        /// </summary>
        [Display(Name = "Timer Details", Description = "A short summary telling the details of the timer.")]
        public string TimerDetails { get; set; }

        /// <summary>
        /// ID of the task the timer is for, such as water, add fertilizer, general checkup, etc...
        /// </summary>
        [Required]
        [Display(Name = "Task", Description = "The particular task that this timer refers to.")]
        public int TimerTaskID { get; set; }

        public Models.TimerShow ToViewModel(string plantName, string taskName)
        {
            return new Models.TimerShow(this)
            {
                PlantName = plantName ?? "",
                TaskName = taskName ?? ""
            };
        }
        public PlantTimers()
        {

        }
        public PlantTimers(Models.TimerShow t)
        {
            this.id = t.id;
            this.PlantID = t.PlantID;
            this.TimerDetails = t.TimerDetails;
            this.TimerInterval = t.TimerInterval;
            this.TimerOffset = t.TimerOffset;
            this.TimerTaskID = t.TimerTaskID;
        }
    }
    /// <summary>
    /// One time events for notificatiosn
    /// </summary>
    public class PlantAlarms
    {
        [Key]
        public int id { get; set; }

        /// <summary>
        /// ID of the plant the alarm is for.
        /// </summary>
        [Required]
        [Display(Name = "Plant")]
        public int PlantID { get; set; }

        /// <summary>
        /// The DateTime for the activation of the alarm.
        /// I.E. the first time after the planting the timer should be called, will then use interval afterwords.
        /// </summary>
        public TimeSpan AlarmTime { get; set; }

        /// <summary>
        /// Description of what to do when this alarm activates.
        /// </summary>
        [Display(Name = "Alarm Details", Description = "A short summary telling the details of the alarm.")]
        public string AlarmDetails { get; set; }

        /// <summary>
        /// ID of the task the alarm is for, such as water, add fertilizer, general checkup, etc...
        /// </summary>
        [Required]
        [Display(Name = "Task")]
        public int AlarmTaskID { get; set; }


        public PlantAlarms()
        {

        }
        public PlantAlarms(Models.AlarmShow a)
        {
            this.id = a.id;
            this.PlantID = a.PlantID;
            this.AlarmDetails = a.AlarmDetails;
            this.AlarmTaskID = a.AlarmTaskID;
            this.AlarmTime = a.AlarmTime;
        }

        public Models.AlarmShow ToViewModel(string plantName, string taskName)
        {
            return new Models.AlarmShow(this)
            {
                PlantName = plantName ?? "",
                TaskName = taskName ?? ""
            };
        }
    }
    /// <summary>
    /// Specific tasks with details
    /// </summary>
    public class PlantTasks
    {
        /* plant
         * water
         * add fertilizer
         * add food
         * harvest
         * weed
         */
        [Key]
        public int id { get; set; }

        /// <summary>
        /// The name of the task
        /// </summary>
        [Display(Name = "Task Name", Description = "The name of the task.")]
        public string TaskName { get; set; }

        /// <summary>
        /// Description of what to do when the task is requested, such as water, harvest, fertilizer, weed, etc...
        /// </summary>
        [Display(Name = "Description", Description = "A description / information of what to do for this task.")]
        public string TaskDescription { get; set; }

        [Display(Name = "Sort Group", Description = "A string that is used to identify the group the task belongs to. Used for sorting in the drop down.")]
        public string SortGroup { get; set; }
    }
    #endregion

    #region USER DATA

    public class Garden
    {
        [Key]
        public int id { get; set; }

        [Display(Name = "Garden Name", Description = "The Garden Name / Identifier. Must be unique per user.")]
        public string Name { get; set; }

        public string UserID { get; set; }

        public Models.GardenShow ToViewModel(int PlotCount)
        {
            return new Models.GardenShow(this,PlotCount);
        }
        public Garden()
        {

        }
        public Garden(Models.GardenShow g = null)
        {
            this.id = g.id;
            this.Name = g.Name;
            this.UserID = g.UserID;
        }
    }

    public class Plot
    {
        [Key]
        public int id { get; set; }
        
        [Display(Name = "Plot Name")]
        public string Name { get; set; }

        [Required]
        public int GardenID { get; set; }

        public int PlantID { get; set; }

        [Display(Name = "Size (Area)", Description = "The size of the plot in meters squared.")]
        public double PlotArea { get; set; }

        [Display(Name = "Start time", Description = "The Start time for this plot.")]
        public DateTime StartTime { get; set; }

        [Display(Name = "Seed count", Description = "The intial count of seeds planted at the start time.")]
        public int InitialSeedCount { get; set; }

        public Models.PlotShow ToViewModel(string GardenName, string PlantName)
        {
            return new Models.PlotShow(this, GardenName, PlantName);
        }
        public Plot()
        {

        }
        public Plot(Models.PlotShow p = null)
        {
            this.id = p.id;
            this.Name = p.Name;
            this.GardenID = p.GardenID;
            this.PlantID = p.PlantID;
            this.PlotArea = p.PlotArea;
            this.StartTime = p.StartTime;
            this.InitialSeedCount = p.InitialSeedCount;
        }
    }

    public class ProgressLog
    {
        [Key]
        public int id { get; set; }

        [Required]
        public int PlotID { get; set; }

        public DateTime LogTime { get; set; }

        public int SeedGerminationCount { get; set; }

        public string LogNotes { get; set; }
    }

    [Serializable]
    public class ScheduledNotification
    {
        [Key]
        public long id { get; set; }
        
        [Required]
        public string UserID { get; set; }
        
        public int PlotID { get; set; }

        /// <summary>
        /// Timer:
        ///     Start time + offset + intervals
        /// Alarm:
        ///     Start time + offset
        /// </summary>
        
        public EventTypes EventType { get; set; }
        public int EventID { get; set; }

        public short EmailTemplateId { get; set; }
        
        public string HangFireJobID { get; set; }
    }

    #endregion
    
    /// <summary>
    /// How the harvesting of the plant should be handeled (by computer).
    /// </summary>
    public enum HarvestTypes
    {
        /// <summary>
        /// Fully harvesting the plant. Requires complete replanting afterwords.
        /// </summary>
        FULLHARVEST = 0,
        /// <summary>
        /// Only picking peices off the plant, such as fruit. Does not require replanting.
        /// </summary>
        PICKOFF = 1
    }

    public enum EventTypes
    {
        ALARM = 0,
        TIMER = 1,
        MISC = 2
    }

    public class EnumBoolIndexer<T>
    {
        private readonly List<T> keys;
        private bool[] array;

        public EnumBoolIndexer(params T[] enabled)
        {
            keys = Enum.GetValues(typeof(T)).Cast<T>().ToList();
            array = new bool[keys.Count];

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = enabled.Contains(keys[i]);
            }
        }

        public bool this[T index]
        {
            get { return array[keys.IndexOf(index)]; }
            set { array[keys.IndexOf(index)] = value; }
        }
    }
}
