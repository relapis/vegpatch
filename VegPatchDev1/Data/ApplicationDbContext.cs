﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VegPatchDev1.Models;
using VegPatchDev1;

namespace VegPatchDev1.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        //Data structures
        public DbSet<PlantData> PlantData { get; set; }
        public DbSet<PlantTimers> PlantTimers { get; set; }
        public DbSet<PlantAlarms> PlantAlarms { get; set; }
        public DbSet<PlantTasks> PlantTasks { get; set; }

        //User structures
        public DbSet<Garden> UserGardens { get; set; }
        public DbSet<Plot> UserPlots { get; set; }
        public DbSet<ProgressLog> ProgressLogs { get; set; }
        public DbSet<ScheduledNotification> ScheduledNotifications { get; set; }
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            Extensions._context = this;
            Extensions.DropDownLists._context = this;
            NotificationHandler._context = this;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

    }
}
