﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using VegPatchDev1.Data;

namespace VegPatchDev1
{
    public static class Extensions
    {
        public static ApplicationDbContext _context { get; set; }

        public static string GetUserId(this IPrincipal principal)
        {
            var claimsIdentity = (ClaimsIdentity)principal.Identity;
            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);
            if (claim == null) return "";
            return claim.Value;
        }
        public static bool UserHasAccessToPlot(this IPrincipal principal, int PlotID)
        {
            return (from p in _context.UserPlots
                    join g in _context.UserGardens
                    on p.GardenID equals g.id
                    where p.id.Equals(PlotID) && g.UserID.Equals(principal.GetUserId())
                    select p.Name).FirstOrDefault() != null;
        }
        public static bool UserHasAccessToGarden(this IPrincipal principal, int GardenID)
        {
            return (from g in _context.UserGardens
                    where g.id.Equals(GardenID) && g.UserID.Equals(principal.GetUserId())
                    select g.Name).FirstOrDefault() != null;
        }
        public static bool CanBeCreated(this Garden garden)
        {
            return !_context.UserGardens.Any(g => g.UserID.Equals(garden.UserID) && g.Name.Equals(garden.Name));
        }

        public static IHtmlContent DisplayDescriptionFor<TModel, TValue>(this IHtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            return html.DisplayMetadataFor(expression, (m) => m.Description) as HtmlString;
        }

        public static IHtmlContent DisplayMetadataFor<TModel, TValue>(this IHtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, Func<ModelMetadata, string> GetMetadata)
        {
            if (html == null) throw new ArgumentNullException(nameof(html));
            if (expression == null) throw new ArgumentNullException(nameof(expression));
            if (GetMetadata == null) throw new ArgumentNullException("GetMetadata Func<ModelMetadata, string>");

            var modelExplorer = ExpressionMetadataProvider.FromLambdaExpression(expression, html.ViewData, html.MetadataProvider);
            if (modelExplorer == null) throw new InvalidOperationException($"Failed to get model explorer for {ExpressionHelper.GetExpressionText(expression)}");
            
            return new HtmlString(GetMetadata(modelExplorer.Metadata));
        }

        public static IEnumerable<T> CastTo<F, T>(this IEnumerable<F> array)
        {
            foreach (object o in array)
            {
                yield return (T)o;
            }
        }

        public static IEnumerable<V> GetValues<K, V>(this IEnumerable<KeyValuePair<K, V>> self)
        {
            foreach (var v in self)
            {
                yield return v.Value;
            }
        }
        public static IEnumerable<K> GetKeys<K, V>(this IEnumerable<KeyValuePair<K, V>> self)
        {
            foreach (var v in self)
            {
                yield return v.Key;
            }
        }


        public static class DropDownLists
        {
            public static ApplicationDbContext _context { get; set; }

            public static Task<IEnumerable<SelectListItem>> GetTaskDropDownListAsync(int? PlantID = null)
            {
                GroupGenerator gen = new GroupGenerator(_context.PlantTasks.Select((t) => t.SortGroup).ToArray());

                return Task.Run(() => CreateDropDownList(
                    _context.PlantTasks.ToList(),
                    (t) => t.id.ToString(),
                    (t) => t.TaskName,
                    (t) => gen.GetGroup(t.SortGroup)));
            }

            public static Task<IEnumerable<SelectListItem>> GetGardenDropListAsync(IPrincipal User)
            {
                return Task.Run(() => CreateDropDownList(
                    _context.UserGardens.Where((g) => g.UserID.Equals(User.GetUserId())).ToList(),
                    (g) => g.id.ToString(),
                    (g) => g.Name));
            }
            public static Task<IEnumerable<SelectListItem>> GetPlantDropListAsync()
            {
                SelectListGroup Weeds = new SelectListGroup { Name = "Weeds" };
                SelectListGroup Plants = new SelectListGroup { Name = "Plants" };

                return Task.Run(() => CreateDropDownList(
                    _context.PlantData.ToList(),
                    (p) => p.id.ToString(),
                    (p) => p.PlantName,
                    (p) => p.IsAWeed ? Weeds : Plants));
            }
            public static IEnumerable<SelectListItem> CreateDropDownList<T>(IEnumerable<T> list, Func<T, string> GetValue, Func<T, string> GetText, Func<T, SelectListGroup> GetGroup = null)
            {
                foreach (T item in list)
                {
                    var ret = new SelectListItem()
                    {
                        Value = GetValue(item),
                        Text = GetText(item)
                    };
                    if (GetGroup != null)
                    {
                        ret.Group = GetGroup(item);
                    }
                    yield return ret;
                }
            }

            public class GroupGenerator
            {
                List<SelectListGroup> groups;

                public GroupGenerator(params string[] startingGroups)
                {
                    groups = new List<SelectListGroup>();
                    foreach(string s in startingGroups)
                    {
                        NewGroup(s);
                    }
                }
                SelectListGroup StringToGroup(string name)
                {
                    return new SelectListGroup()
                    {
                        Name = name
                    };
                }
                SelectListGroup NewGroup(string name)
                {
                    SelectListGroup ret = StringToGroup(name);
                    groups.Add(ret);
                    return ret;
                }
                bool ContainsGroup(string name)
                {
                    return groups.Any((s) => s.Name.ToUpper() == name.ToUpper());
                }
                SelectListGroup GetFirst(string name)
                {
                    return groups.FirstOrDefault((s) => s.Name.ToUpper() == name.ToUpper());
                }
                public SelectListGroup GetGroup(string name)
                {
                    SelectListGroup ret = GetFirst(name);
                    if (ret == null)
                    {
                        return NewGroup(name);
                    }
                    else
                    {
                        return ret;
                    }
                }
            }
        }
    }


}
