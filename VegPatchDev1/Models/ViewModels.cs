﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VegPatchDev1.Data;

namespace VegPatchDev1.Models
{
    #region FORM MODELS

    public class TimerShow : PlantTimers
    {
        [Display(Name = "Initial Time", Description = "Initial wait time after planting the plant hours")]
        public int InputOffset { get; set; }
        
        [Display(Name = "Interval Time", Description = "Interval wait time after the previous timer in hours")]
        public int InputInterval { get; set; }
        
        [Display(Name = "Plant Name")]
        public string PlantName { get; set; }

        [Display(Name = "Task Name")]
        public string TaskName { get; set; }

        public TimerShow()
        {

        }
        public TimerShow(PlantTimers t)
        {
            FromBase(t);
            InputOffset = Convert.ToInt32(t.TimerOffset.TotalHours);
            InputInterval = Convert.ToInt32(t.TimerInterval.TotalHours);
        }

        private void FromBase(PlantTimers t)
        {
            this.id = t.id;
            this.PlantID = t.PlantID;
            this.TimerDetails = t.TimerDetails;
            this.TimerOffset = t.TimerOffset;
            this.TimerInterval = t.TimerInterval;
            this.TimerTaskID = t.TimerTaskID;
        }

        public PlantTimers ToBase()
        {
            TimerOffset = new TimeSpan(InputOffset, 0, 0);
            TimerInterval = new TimeSpan(InputInterval, 0, 0);
            return this;
        }
    }

    public class AlarmShow : PlantAlarms
    {
        [Display(Name = "Time", Description ="The days after the start of the plant when the alarm should activate")]
        public int InputTime { get; set; }

        [Display(Name = "Plant", Description = "The plant this alarm refers to.")]
        public string PlantName { get; set; }

        [Display(Name = "Task", Description = "The particular task that applies to this alarm.")]
        public string TaskName { get; set; }

        public AlarmShow()
        {

        }
        public AlarmShow(PlantAlarms a)
        {
            FromBase(a);
            InputTime = Convert.ToInt32(a.AlarmTime.TotalDays);
        }
        private void FromBase(PlantAlarms a)
        {
            this.id = a.id;
            this.PlantID = a.PlantID;
            this.AlarmTime = a.AlarmTime;
            this.AlarmDetails = a.AlarmDetails;
            this.AlarmTaskID = a.AlarmTaskID;
        }
        public PlantAlarms ToBase()
        {
            AlarmTime = new TimeSpan(InputTime, 0, 0, 0);
            return new PlantAlarms(this);
        }
    }

    public class GardenShow : Garden
    {
        [Display(Name = "Plot Count", Description = "The amount of plots belonging to this garden")]
        public int PlotCount { get; set; }

        public GardenShow()
        {

        }
        public GardenShow(Garden g, int plotCount)
        {
            FromBase(g);
            PlotCount = plotCount;
        }
        private void FromBase(Garden g)
        {
            this.id = g.id;
            this.Name = g.Name;
            this.UserID = g.UserID;
        }

        public Garden ToBase()
        {
            return new Garden(this);
        }
    }

    public class PlotShow : Plot
    {

        [Display(Name = "Garden Name")]
        public string GardenName { get; set; }

        [Display(Name = "Plant Name")]
        public string PlantName { get; set; }

        public PlotShow()
        {

        }
        public PlotShow(Plot p, string gardenName, string plantName)
        {
            FromBase(p);
            GardenName = gardenName;
            PlantName = plantName;
        }
        private void FromBase(Plot p)
        {
            this.id = p.id;
            this.Name = p.Name;
            this.PlantID = p.PlantID;
            this.GardenID = p.GardenID;
            this.InitialSeedCount = p.InitialSeedCount;
            this.PlotArea = p.PlotArea;
            this.StartTime = p.StartTime;
        }
        public Plot ToBase()
        {
            return new Plot(this);
        }
    }
    #endregion
}
